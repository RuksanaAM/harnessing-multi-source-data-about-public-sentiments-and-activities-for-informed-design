package com.example.venuereview;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.util.ArrayList;

public class SIGNUP extends AppCompatActivity implements View.OnClickListener {
    Button b1;
    EditText e1, e2, e3, e4, e5, e6, e7, e8, e9, e10;
    TextView t1, t2;
    RadioButton r1, r2;
    SharedPreferences s;
    String url = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        b1 = (Button) findViewById(R.id.button4);
        e1 = (EditText) findViewById(R.id.editTextTextPersonName5);
        e2 = (EditText) findViewById(R.id.editTextTextPersonName4);
        e3 = (EditText) findViewById(R.id.editTextTextPersonName6);
        e4 = (EditText) findViewById(R.id.editTextTextPersonName7);
        e5 = (EditText) findViewById(R.id.editTextTextPersonName8);
        e6 = (EditText) findViewById(R.id.editTextTextPersonName9);
        e7 = (EditText) findViewById(R.id.editTextTextPersonName10);
        e8 = (EditText) findViewById(R.id.editTextTextPersonName11);
        e9 = (EditText) findViewById(R.id.editTextTextPersonName12);
        e10 = (EditText) findViewById(R.id.editTextTextPersonName13);
        t1 = (TextView) findViewById(R.id.textView11);
        t2 = (TextView) findViewById(R.id.textView12);
        r1 = (RadioButton) findViewById(R.id.radioButton);
        r2 = (RadioButton) findViewById(R.id.radioButton2);
        b1.setOnClickListener(this);
        s = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        url = s.getString("url", "") + "and_signup";

    }

    @Override
    public void onClick(View v) {
        if (v == b1) {
            String name = e1.getText().toString();
            String dob = e2.getText().toString();
            String gender ="";
            String phone = e3.getText().toString();
            String email = e4.getText().toString();
            String pin = e7.getText().toString();
            String place = e6.getText().toString();
            String city = e5.getText().toString();
            String street = e8.getText().toString();
            String password = e9.getText().toString();
            String cpassword = e10.getText().toString();
            if (r1.isChecked()){
                gender=r1.getText().toString();
            }
            if (r2.isChecked()){
                gender=r2.getText().toString();
            }
            int f=0;
            if (name.equalsIgnoreCase("")){
                f=1;
                e1.setError("Enter Name");
            }
            if (dob.equalsIgnoreCase("")){
                f=1;
                e2.setError("Enter Date Of Birth");
            }
            if (phone.equalsIgnoreCase("")){
                f=1;
                e3.setError("Enter Phone No");
            }
            else  if(phone.length()!=10)
            {
                f=1;
                e3.setError("invalid phone number");
            }
            if(!android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches())
            {
                f=1;
                e4.setError("invalid email id");
            }
            if(pin.length()!=6)
            {
                f=1;
                e7.setError("invalid pin number");
            }
            if (place.equalsIgnoreCase("")){
                f=1;
                e6.setError("Enter Place");
            }
            if (city.equalsIgnoreCase("")){
                f=1;
                e5.setError("Enter City");
            }
            if (street.equalsIgnoreCase("")){
                f=1;
                e8.setError("Enter Street");
            }
            if (password.equalsIgnoreCase("")){
                f=1;
                e9.setError("Set Password");
            }
            if (cpassword.equalsIgnoreCase("")){
                f=1;
                e10.setError("Confirm Password");
            }
            if (!cpassword.equalsIgnoreCase(password)) {
                f=1;
                Toast.makeText(this, "password missmatch", Toast.LENGTH_SHORT).show();
            }
            if (gender.equalsIgnoreCase("")){
                f=1;
                Toast.makeText(this, "choose gender", Toast.LENGTH_SHORT).show();
            }
            if (f==0) {
                try {
                    ArrayList<NameValuePair> param = new ArrayList<>();
                    param.add(new BasicNameValuePair("name", name));
                    param.add(new BasicNameValuePair("dob", dob));
                    param.add(new BasicNameValuePair("gender", gender));
                    param.add(new BasicNameValuePair("phone", phone));
                    param.add(new BasicNameValuePair("email", email));
                    param.add(new BasicNameValuePair("pin", pin));
                    param.add(new BasicNameValuePair("place", place));
                    param.add(new BasicNameValuePair("city", city));
                    param.add(new BasicNameValuePair("street", street));
                    param.add(new BasicNameValuePair("password", password));

                    JSONParser parser = new JSONParser();
                    JSONObject jsonObject = new JSONObject();
                    jsonObject = parser.makeHttpRequest(url, "POST", param);
                    String status = jsonObject.getString("status");

                    if (status.equalsIgnoreCase("1")) {
                        Toast.makeText(this, "success", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(SIGNUP.this, LOGIN.class));
                    } else {
                        Toast.makeText(this, "ERROR", Toast.LENGTH_SHORT).show();

                    }

                } catch (Exception e) {
                    Toast.makeText(this, e.toString(), Toast.LENGTH_SHORT).show();
                }
            }
        }

    }
}
