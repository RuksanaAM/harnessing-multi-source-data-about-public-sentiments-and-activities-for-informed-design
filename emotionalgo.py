
import pandas as pd
import numpy as np
import nltk
import re
import itertools
import time



start_time = time.time()
import os


#stopset = set(stopwords.words('english'))

from nltk.stem.wordnet import WordNetLemmatizer 
lem = WordNetLemmatizer()
class emotional:
    #comprehensive cleaning
    def cleaning(self,text):
        txt = str(text)
        txt = re.sub(r"http\S+", "", txt)
        if len(txt) == 0:
            return 'no text'
        else:
            txt = txt.split()
            index = 0
            for j in range(len(txt)):
                if txt[j][0] == '@':
                    index = j
            txt = np.delete(txt, index)
            if len(txt) == 0:
                return 'no text'
            else:
                words = txt[0]
                for k in range(len(txt)-1):
                    words+= " " + txt[k+1]
                txt = words
                txt = re.sub(r'[^\w]', ' ', txt)
                if len(txt) == 0:
                    return 'no text'
                else:
                    txt = ''.join(''.join(s)[:2] for _, s in itertools.groupby(txt))
                    txt = txt.replace("'", "")
                    txt = nltk.tokenize.word_tokenize(txt)
                    # data.content[i] = [w for w in data.content[i] if not w in stopset]
                    for j in range(len(txt)):
                        txt[j] = lem.lemmatize(txt[j], "v")
                    if len(txt) == 0:
                        return 'no text'
                    else:
                        return txt


    def lik(self,msg):
        data = pd.read_csv('C:\\Users\\SSD\\PycharmProjects\\venuereview\\static\\data-train.xls')
        # data = pd.read_excel('C:\\Users\\nabil\\PycharmProjects\\untitled2\\sample_data_2.xlsx', engine='openpyxl')
        data = data.iloc[:500, :]

        data['Text'] = data['Text'].map(lambda x: self.cleaning(x))

        data = data.reset_index(drop=True)
        for i in range(len(data)):
            words = data.Emotion[i][0]
            for j in range(len(data.Text[i]) - 1):
                words += ' ' + data.Text[i][j + 1]
            data.Text[i] = words

        x = int(np.round(len(data) * 0.75))
        train = data.iloc[:x, :].reset_index(drop=True)
        test = data.iloc[x:, :].reset_index(drop=True)

        from textblob.classifiers import NaiveBayesClassifier as NBC

        training_corpus = []

        for k in range(len(train)):
            training_corpus.append((train.Text[k], train.Emotion[k]))

        # test_corpus = []

        # for l in range(len(test)):
        #     test_corpus.append((test.content[l], test.sentiment[l]))

        model = NBC(training_corpus)

        # print(model.accuracy(test_corpus))
        # s=model.classify("I am very gloomy. I Miss u")
        # print(s)
        # s=model.classify("Dont make a promise if keep it")
        # print(s)
        # s=model.classify("i love you")
        # print(s)
        s=model.classify(msg)
        print(s)
        return s

# lik(" When I have to leave the baby in the carriage and go shopping, I fear that something will happen to the baby.  ")
# lik("am going to suicide")