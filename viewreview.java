package com.example.venuereview;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.util.ArrayList;

public class viewreview extends AppCompatActivity implements View.OnClickListener {
    ListView l1;
    SharedPreferences s;
    String url,url2;
    FloatingActionButton fl;
  int count=0;
  Handler hnd=new Handler();
    Runnable ad;

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        count=0;
        startActivity(new Intent(getApplicationContext(),HOME_.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_viewreview);
//        l1 = (ListView) findViewById(R.id.review);









        PieChart pieChart = findViewById(R.id.piechart);
        fl = (FloatingActionButton) findViewById(R.id.floatingActionButton);

        fl.setOnClickListener(this);
        s = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        url = s.getString("url", "") + "and_view_review";
        try {
            ArrayList<NameValuePair> param = new ArrayList<>();
//            param.add(new BasicNameValuePair("review", review));
            param.add(new BasicNameValuePair("vid", s.getString("vid", "")));
//            param.add(new BasicNameValuePair("lid", s.getString("lid", "")));

            JSONParser parser = new JSONParser();
            JSONObject jsonObject = new JSONObject();
            jsonObject = parser.makeHttpRequest(url, "POST", param);
            String status = jsonObject.getString("status");

            if (status.equalsIgnoreCase("1")) {
                Toast.makeText(this, "success", Toast.LENGTH_SHORT).show();
                String neutral= jsonObject.getString("neutral");
                String sad= jsonObject.getString("sad");
                String fear= jsonObject.getString("fear");
                String joy= jsonObject.getString("joy");
                String anger= jsonObject.getString("anger");
                float neu = Float.parseFloat(neutral);
                float sa = Float.parseFloat(sad);
                float fea = Float.parseFloat(fear);
                float jo = Float.parseFloat(joy);
                float ang = Float.parseFloat(anger);

                ArrayList NoOfEmp = new ArrayList();

                NoOfEmp.add(new Entry(ang, 0));
                NoOfEmp.add(new Entry(fea, 1));
                NoOfEmp.add(new Entry(neu, 2));
                NoOfEmp.add(new Entry(jo, 3));
                NoOfEmp.add(new Entry(sa, 4));
//                NoOfEmp.add(new Entry(1487f, 5));
//                NoOfEmp.add(new Entry(1501f, 6));
//                NoOfEmp.add(new Entry(1645f, 7));
//                NoOfEmp.add(new Entry(1578f, 8));
//                NoOfEmp.add(new Entry(1695f, 9));
                PieDataSet dataSet = new PieDataSet(NoOfEmp, "Emotion %");

                ArrayList year = new ArrayList();

                year.add("anger");
                year.add("fear");
                year.add("neutral");
                year.add("joy");
                year.add("sad");
//                year.add("2013");
//                year.add("2014");
//                year.add("2015");
//                year.add("2016");
//                year.add("2017");
                PieData data = new PieData(year, dataSet);
                pieChart.setData(data);
                dataSet.setColors(ColorTemplate.COLORFUL_COLORS);
                pieChart.animateXY(5000, 5000);
                loc_check();
//                startActivity(new Intent(viewreview.this, Homepage.class));
            }
            else {
                Toast.makeText(this, "error", Toast.LENGTH_SHORT).show();

            }
        } catch (Exception e) {
//            Toast.makeText(this, e.toString(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View v) {
        startActivity(new Intent(viewreview.this, review.class));
    }

    public void loc_check(){
        SharedPreferences s=PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        String statuscheck=s.getString("status","");
        if(statuscheck.equalsIgnoreCase("1")) {

            s = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            url2 = s.getString("url", "") + "location_check";
            try {
                ArrayList<NameValuePair> param = new ArrayList<>();
//            param.add(new BasicNameValuePair("review", review));
                param.add(new BasicNameValuePair("vid", s.getString("vid", "")));
                param.add(new BasicNameValuePair("lat", LocationService.lati));
                param.add(new BasicNameValuePair("lon", LocationService.logi));
//            param.add(new BasicNameValuePair("lid", s.getString("lid", "")));

                JSONParser parser = new JSONParser();
                JSONObject jsonObject = new JSONObject();
                jsonObject = parser.makeHttpRequest(url2, "POST", param);
                String status2 = jsonObject.getString("status");

                if (status2.equalsIgnoreCase("1")) {
                    String val = jsonObject.getString("val");
                    if (val.equalsIgnoreCase("1")) {
//                        Toast.makeText(this, "AAAAAAAAAAAAAAAAAAAAAAAA", Toast.LENGTH_SHORT).show();
                        ad = new Runnable() {
                            @Override
                            public void run() {
                                count = count + 1;
//                Toast.makeText(viewreview.this, "count=="+count, Toast.LENGTH_SHORT).show();
                                if (count == 10) {
//    hnd(ad);
                                    hnd.removeCallbacks(ad);
                                    Toast.makeText(viewreview.this, "Stop handler", Toast.LENGTH_SHORT).show();
                                    startActivity(new Intent(getApplicationContext(), review.class));
                                }
                                hnd.postDelayed(ad, 1000);
                            }
                        };


                        hnd.post(ad);


                    }


                } else {
                    Toast.makeText(this, "Not Exact Place", Toast.LENGTH_SHORT).show();

                }
            } catch (Exception e) {
            Toast.makeText(this, e.toString(), Toast.LENGTH_SHORT).show();
            }
        }
    }
}