package com.example.venuereview;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.util.ArrayList;

public class Profile extends AppCompatActivity {
    TextView tvname,tvgender,tvplace,tvpost,tvdistrict,tvphone,tvemail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        tvname=(TextView) findViewById(R.id.name);
        tvgender=(TextView) findViewById(R.id.gender);
        tvplace=(TextView) findViewById(R.id.place);
        tvpost=(TextView) findViewById(R.id.post);
        tvdistrict=(TextView) findViewById(R.id.district);
        tvphone=(TextView) findViewById(R.id.phone);
        tvemail=(TextView) findViewById(R.id.email);


        try{

            SharedPreferences sh = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            String hu = sh.getString("ip", "");
            String url = sh.getString("url","")+"profile";;


            ArrayList<NameValuePair> param = new ArrayList<>();
            param.add(new BasicNameValuePair("lid", sh.getString("lid","")));
//        param.add(new BasicNameValuePair("password", password));

            JSONParser parser = new JSONParser();
            JSONObject jsonObject = new JSONObject();
            jsonObject = parser.makeHttpRequest(url, "POST", param);
            String status = jsonObject.getString("status");

            Toast.makeText(this, "=="+jsonObject, Toast.LENGTH_SHORT).show();
            if (status.equalsIgnoreCase("1")) {
//            String lid = jsonObject.getString("lid");
                Toast.makeText(this, "name=="+jsonObject.getString("name"), Toast.LENGTH_SHORT).show();
                String name=jsonObject.getString("name").toString();
                tvname.setText(name);
            tvgender.setText(jsonObject.getString("gender"));
            tvplace.setText(jsonObject.getString("place"));
            tvpost.setText(jsonObject.getString("post"));
            tvdistrict.setText(jsonObject.getString("district"));
            tvemail.setText(jsonObject.getString("email"));
            tvphone.setText(jsonObject.getString("phone"));


            } else {
                Toast.makeText(this, "Invalid username or password", Toast.LENGTH_SHORT).show();

            }

        } catch (Exception e) {
            Toast.makeText(this, e.toString(), Toast.LENGTH_SHORT).show();
        }

    }
}