package com.example.venuereview;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class Custom_venue extends BaseAdapter {

    private android.content.Context Context;
    ArrayList<String> name,description,image,lati,longi,vid;


    public Custom_venue(android.content.Context applicationContext, ArrayList<String> name, ArrayList<String> description,ArrayList<String> image,ArrayList<String> lati,ArrayList<String> longi,ArrayList<String> vid) {

        this.Context=applicationContext;
        this.name=name;
        this.description=description;
        this.image=image;
        this.lati=lati;
        this.longi=longi;
        this.vid=vid;

    }

    @Override
    public int getCount() {

        return name.size();
    }

    @Override
    public Object getItem(int arg0) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public long getItemId(int arg0) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public View getView(int position, View convertview, ViewGroup parent) {


        LayoutInflater inflator=(LayoutInflater)Context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View gridView;
        if(convertview==null)
        {
            gridView=new View(Context);
            gridView=inflator.inflate(R.layout.activity_custom_venue, null);

        }
        else
        {
            gridView=(View)convertview;

        }

        TextView tv1=(TextView)gridView.findViewById(R.id.name);
        TextView tv2=(TextView)gridView.findViewById(R.id.des);
        ImageView tv3=(ImageView)gridView.findViewById(R.id.imageView2);
        final Button img3=(Button)gridView.findViewById(R.id.exit1);
        final Button img4=(Button)gridView.findViewById(R.id.exit2);
        final Button in=(Button)gridView.findViewById(R.id.exit3);
        final Button out=(Button)gridView.findViewById(R.id.exit4);
//        String status="0";

//        if(status.equalsIgnoreCase("0")) {

//            in.setVisibility(View.VISIBLE);
            out.setVisibility(View.INVISIBLE);
//        }
//        else {
//            out.setVisibility(View.VISIBLE);
//            out.setVisibility(View.INVISIBLE);
//        }


        in.setTag(vid.get(position));
        in.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences sh=PreferenceManager.getDefaultSharedPreferences(Context);
                SharedPreferences.Editor ed=sh.edit();
                ed.putString("status","1");
                ed.putString("vid",v.getTag().toString());
                ed.commit();

                Intent ij=new Intent(Context,viewreview.class);
                ij.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                Context.startActivity(ij);
            }
        });
        out.setTag(vid.get(position));
        out.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        try
        {
            SharedPreferences sh=PreferenceManager.getDefaultSharedPreferences(Context);
            String url1=sh.getString("url","")+"static/venuepic/"+image.get(position);
            Picasso.with(Context).load(url1.toString()).memoryPolicy(MemoryPolicy.NO_CACHE).networkPolicy(NetworkPolicy.NO_CACHE).transform(new CircleTransform()).error(R.drawable.ic_menu_gallery).into(tv3);

        }catch (Exception e)
        {
            Toast.makeText(Context, e.toString(), Toast.LENGTH_SHORT).show();
        }

        img4.setTag(vid.get(position));
        img4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences sh= PreferenceManager.getDefaultSharedPreferences(Context);
                SharedPreferences.Editor e = sh.edit();
                e.putString("vid", img4.getTag().toString());//queue_status_id
                e.commit();
                Intent ij=new Intent(Context,viewreview.class);
                ij.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                Context.startActivity(ij);

            }
        });
        img3.setTag(lati.get(position)+"#"+longi.get(position));
        img3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String a=img3.getTag().toString();
                String ab[]=a.split("#");
                String lati=ab[0];
                String longi=ab[1];
                Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                        Uri.parse("http://maps.google.com/maps?q="+lati+","+longi));
                intent .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                Context.startActivity(intent);

            }
        });

        tv1.setTextColor(Color.BLACK);
        tv2.setTextColor(Color.BLACK);
        tv1.setText(name.get(position));
        tv2.setText(description.get(position));

        return gridView;
    }


}
