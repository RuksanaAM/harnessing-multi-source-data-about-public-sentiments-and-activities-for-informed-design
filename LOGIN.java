package com.example.venuereview;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.util.ArrayList;

public class LOGIN extends AppCompatActivity implements View.OnClickListener {
    Button b1;
    Button b2;
    EditText e1;
    EditText e2;
    SharedPreferences s;
    String url="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        b1=(Button)findViewById(R.id.button2);
        b2=(Button)findViewById(R.id.button3);
        e1=(EditText)findViewById(R.id.editTextTextPersonName3);
        e2=(EditText)findViewById(R.id.editTextTextPersonName2);
        b1.setOnClickListener(this);
        b2.setOnClickListener(this);
        s= PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        url=s.getString("url","")+"and_login";
        Log.d("=====================",url);

    }

    @Override
    public void onClick(View v) {
        if (v==b1) {
            String email = e1.getText().toString();
            String password = e2.getText().toString();
            int f = 0;
            if (email.equalsIgnoreCase("")) {
                f = 1;
                e1.setError("Enter Email");
            }
            if (password.equalsIgnoreCase("")) {
                f = 1;
                e2.setError("Enter Password");
            }
            if (f == 0) {
                try {
                    ArrayList<NameValuePair> param = new ArrayList<>();
                    param.add(new BasicNameValuePair("username", email));
                    param.add(new BasicNameValuePair("password", password));

                    JSONParser parser = new JSONParser();
                    JSONObject jsonObject = new JSONObject();
                    jsonObject = parser.makeHttpRequest(url, "POST", param);
                    String status = jsonObject.getString("status");

                    if (status.equalsIgnoreCase("1")) {
                        String lid = jsonObject.getString("lid");
                        SharedPreferences.Editor ed = s.edit();
                        ed.putString("lid", lid);
                        ed.commit();
                        startService(new Intent(getApplicationContext(),LocationService.class));
                        startActivity(new Intent(LOGIN.this, HOME_.class));
                    } else {
                        startActivity(new Intent(LOGIN.this, LOGIN.class));
                        Toast.makeText(this, "Invalid username or password", Toast.LENGTH_SHORT).show();

                    }

                } catch (Exception e) {
                    Toast.makeText(this, e.toString(), Toast.LENGTH_SHORT).show();
                }
            }
        }
        else{
            startActivity(new Intent(LOGIN.this,SIGNUP.class));
        }

    }
}