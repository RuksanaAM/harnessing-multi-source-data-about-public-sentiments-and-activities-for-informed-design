package com.example.venuereview;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.util.ArrayList;

public class Custom_category extends BaseAdapter {

    private android.content.Context Context;
    ArrayList<String> name;
    ArrayList<String> id;

    public Custom_category(android.content.Context applicationContext, ArrayList<String> name, ArrayList<String> id) {

        this.Context=applicationContext;
        this.name=name;
        this.id=id;



    }

    @Override
    public int getCount() {

        return name.size();
    }

    @Override
    public Object getItem(int arg0) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public long getItemId(int arg0) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public View getView(int position, View convertview, ViewGroup parent) {


        LayoutInflater inflator=(LayoutInflater)Context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View gridView;
        if(convertview==null)
        {
            gridView=new View(Context);
            gridView=inflator.inflate(R.layout.activity_custom_category, null);

        }
        else
        {
            gridView=(View)convertview;

        }

        TextView tv1=(TextView)gridView.findViewById(R.id.name);
        final Button img3=(Button)gridView.findViewById(R.id.exit);

        img3.setTag(id.get(position));
        img3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences sh= PreferenceManager.getDefaultSharedPreferences(Context);
                SharedPreferences.Editor e = sh.edit();
                e.putString("cid", img3.getTag().toString());//queue_status_id
                e.commit();
                Intent ij=new Intent(Context,venue.class);
                ij.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                Context.startActivity(ij);

            }
        });

        tv1.setTextColor(Color.BLACK);
        tv1.setText(name.get(position));

        return gridView;
    }

}
